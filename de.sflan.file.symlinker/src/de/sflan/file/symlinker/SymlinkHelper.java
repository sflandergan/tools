/*
 * Copyright 2014 Simon FLandergan
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.sflan.file.symlinker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;

/**
 * Helper methods for symlink operations
 * @author simon.flandergan
 *
 */
public class SymlinkHelper {

	public static boolean isSymlinkExisting(Path sourceFile, Path targetFolder) {

		if (!Files.isDirectory(targetFolder)) {
			throw new IllegalArgumentException(MessageFormat.format(
					"Target folder ''{0}'' is not a folder or does not exist",
					targetFolder));
		}

		Path targetFile = targetFolder.resolve(sourceFile.getFileName());
		return Files.isSymbolicLink(targetFile);
	}

	public static boolean createSymlink(Path sourceFile, Path targetFolder)
			throws IOException {
		checkAccessible(sourceFile, targetFolder);

		Path targetFile = targetFolder.resolve(sourceFile.getFileName());
		if (!Files.exists(targetFile)) {
			Files.createSymbolicLink(targetFile, sourceFile);
			return true;
		}

		return false;
	}

	private static void checkAccessible(Path sourceFile, Path targetFolder) throws IOException {
		if (!Files.exists(sourceFile)) {
			throw new IOException(MessageFormat.format(
					"Source file ''{0}'' does not exist", sourceFile));
		}

		if (!Files.isDirectory(targetFolder)) {
			throw new IOException(MessageFormat.format(
					"Target folder ''{0}'' is not a folder or does not exist",
					targetFolder));
		}
	}

	public static boolean removeSymlink(Path sourceFile, Path targetFolder)
			throws IOException {

		Path targetFile = targetFolder.resolve(sourceFile.getFileName());
		return Files.deleteIfExists(targetFile);

	}

}
