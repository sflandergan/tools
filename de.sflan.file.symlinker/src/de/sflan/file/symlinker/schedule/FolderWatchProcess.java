/*
 * Copyright 2014 Simon FLandergan
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.sflan.file.symlinker.schedule;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.sflan.file.symlinker.ConfigUtil;
import de.sflan.file.symlinker.SymlinkHelper;
import de.sflan.file.symlinker.config.SyncPair;

/**
 * Process handling synchronization of one folder pair Uses NIO WatchServices to
 * keep track of changes on the source folder
 * 
 * @author simon.flandergan
 * 
 */
public class FolderWatchProcess implements Runnable {
	private WatchService ws;
	private WatchKey registrationKey;

	private Path targetFolder;
	private Path sourceFolder;

	private volatile boolean running = true;
	private SyncPair pair;

	private static Logger logger = Logger.getLogger(FolderWatchProcess.class
			.getSimpleName());

	public FolderWatchProcess(SyncPair pair) {
		try {
			this.ws = FileSystems.getDefault().newWatchService();
			this.sourceFolder = Paths.get(pair.getSource());
			this.registrationKey = sourceFolder.register(ws,
					StandardWatchEventKinds.ENTRY_CREATE,
					StandardWatchEventKinds.ENTRY_DELETE);
			this.targetFolder = Paths.get(pair.getTarget());
			this.pair = pair;
		} catch (IOException e) {
			throw new RuntimeException("Failed to create watch service", e);
		}

	}

	@Override
	public void run() {
		while (running) {
			try {
				WatchKey wk = this.ws.poll(15l, TimeUnit.SECONDS);
				if (wk != null) {
					try {
						List<WatchEvent<?>> events = wk.pollEvents();
						for (WatchEvent<?> event : events) {
							if (StandardWatchEventKinds.ENTRY_CREATE
									.equals(event.kind())) {
								/*
								 * resolve full path to source file
								 */
								Path file = this.sourceFolder
										.resolve((Path) event.context());
								if (ConfigUtil.handleFile(file, pair)) {
									handleNewFile(file);
								}
							} else if (StandardWatchEventKinds.ENTRY_DELETE
									.equals(event.kind())) {
								/*
								 * check if we had a symlink to this file
								 */
								Path file = (Path) event.context();
								if (SymlinkHelper.isSymlinkExisting(file, this.targetFolder)){
									handleFileDeleted(file);
								}
							}
						}
					} finally {
						wk.reset();
					}
				}
			} catch (InterruptedException e) {
				/*
				 * ignore we just take the next iteration or break
				 */
			}
		}

		/*
		 * clean-up
		 */
		try {
			this.registrationKey.cancel();
			this.ws.close();
		} catch (IOException e) {
			/*
			 * ignore
			 */
		}
	}

	private void handleFileDeleted(Path file) {
		try {
			if (SymlinkHelper.removeSymlink(file, targetFolder)) {
				logger.info(MessageFormat
						.format("Successfully removed symlink to file ''{0}'' from folder ''{1}''",
								file, targetFolder));
			}
		} catch (IOException e) {
			logger.log(Level.WARNING, "Error deleting symlink", e);
		}
	}

	private void handleNewFile(Path file) {
		try {
			boolean success = SymlinkHelper.createSymlink(file, targetFolder);
			if (success) {
				logger.info(MessageFormat
						.format("Successfully created symlink for file ''{0}'' in folder ''{1}''",
								file, this.targetFolder));
			} else {
				logger.info(MessageFormat
						.format("Failed to creat symlink for file ''{0}'' in folder ''{1}''",
								file, this.targetFolder));
			}
		} catch (IOException e) {
			logger.log(Level.WARNING, "Error creating symlink", e);
		}
	}

	public void stop() {
		this.running = false;
	}

}
