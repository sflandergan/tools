/*
 * Copyright 2014 Simon FLandergan
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.sflan.file.symlinker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.sflan.file.symlinker.config.SyncPair;

/**
 * Process synchronizing a folder pair.
 *  - Creating symlinks for each file not found on the target side
 *  	, but on the source
 *  - Deleting symlinks for each file not found on the source side
 *  	, but already created as symlink on the target side
 * @author simon.flandergan
 *
 */
public class SyncProcess implements Callable<Integer> {
	private SyncPair pair;
	private static Logger logger = Logger.getLogger(SyncProcess.class.getSimpleName());

	public SyncProcess(SyncPair pair){
		this.pair = pair;
	}
	
	@Override
	public Integer call() throws Exception {
		int newSymlinks = 0;
		/*
		 * read source files
		 * and create missing symlinks
		 */
		Path sourcePath = Paths.get(this.pair.getSource());
		for (Path filePath : Files.newDirectoryStream(sourcePath)){
			if (ConfigUtil.handleFile(filePath, pair)){
				if(updateSymlink(filePath)){
					newSymlinks++;
				}
			}
		}
		
		/*
		 * remove dead symlinks
		 */
		Path targetPath = Paths.get(this.pair.getTarget());
		for (Path filePath : Files.newDirectoryStream(targetPath)){ 
			if (Files.isSymbolicLink(filePath)){
				if (! Files.exists(filePath)){
					/*
					 * target file seems to be removed
					 */
					Files.delete(filePath);
					logger.info(MessageFormat.format(
							"Removed dead symlink at location {0}", filePath));
				}
			}
		}
			
		return newSymlinks;
	}

	private boolean updateSymlink(Path filePath) {
		Path targetFolder = Paths.get(this.pair.getTarget());
		boolean result = false;
		
		if (!SymlinkHelper.isSymlinkExisting(filePath, targetFolder)){
			/*
			 * let's create a new symlink
			 */
			try {
				result = SymlinkHelper.createSymlink(filePath, targetFolder);
				if (result){
					logger.info(MessageFormat.format(
							"Successfully created symbolic link for file {0} at folder {1}", filePath, targetFolder));
				}
				
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Failed to create symlink", e);
			}
		}

		return result;
	}


}
