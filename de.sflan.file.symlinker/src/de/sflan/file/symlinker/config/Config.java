
package de.sflan.file.symlinker.config;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for config complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="config">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sync_pair" type="{http://sflan.de/file/symlink/config}sync_pair" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "config", propOrder = {
    "syncPair"
})
public class Config {

    @XmlElement(name = "sync_pair", required = true)
    protected List<SyncPair> syncPair;

    /**
     * Gets the value of the syncPair property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the syncPair property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSyncPair().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SyncPair }
     * 
     * 
     */
    public List<SyncPair> getSyncPair() {
        if (syncPair == null) {
            syncPair = new ArrayList<SyncPair>();
        }
        return this.syncPair;
    }

}
