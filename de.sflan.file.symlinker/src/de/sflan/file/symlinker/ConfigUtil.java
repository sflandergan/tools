/*
 * Copyright 2014 Simon FLandergan
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.sflan.file.symlinker;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import de.sflan.file.symlinker.config.SyncPair;

/**
 * Util class for configuration reading
 * @author simon.flandergan
 *
 */
public class ConfigUtil {
	
	public static boolean handleFile(Path pathToFile, SyncPair pair){
		boolean result = false;
		if (Files.isDirectory(pathToFile) && Boolean.TRUE == pair.isLinkFolders()){
			result =  true;
		}else if (Files.isRegularFile(pathToFile)){
			/*
			 * check if suffix matches or suffix matching not desired
			 */
			List<String> fileSuffix = pair.getFileSuffix();
			if (fileSuffix.isEmpty()){
				result = true;
			}else {
				String filename = pathToFile.getFileName().toString();
				for (String suffix : fileSuffix){
					if (filename.endsWith(suffix)){
						result = true;
						break;
					}
				}
			}
		}
		
		return result;
	}

}
